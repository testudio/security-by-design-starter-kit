# Secure by Design: Resources

This repo contains resources for `Secure by Design: Integrating Security into Development` presentation at DrupalSouth Wellington 2023, updated for Drupal Dev Days Vienna 2023.

- [View slides](https://docs.google.com/presentation/d/1rWU0i-qFKcXRfIxXYFE_8y6Su1rmLVeEO325PfxiE0M/edit?usp=sharing)
- [Video of presentation](https://www.youtube.com/watch?v=2rse2G1xfmE&ab_channel=DrupalAustria)

This resource guide is based on government publications which were designed to promote `secure-by-design` tactics for software developers:
- [ACSC: Guidelines for Software Development](https://www.cyber.gov.au/resources-business-and-government/essential-cyber-security/ism/cyber-security-guidelines/guidelines-software-development)
- [CISA: Secure-by-design](https://www.cisa.gov/resources-tools/resources/secure-by-design-and-default)

And OWASP guidelines:
- [OWASP: Web Security Testing Guide (WSTG)](https://owasp.org/www-project-web-security-testing-guide/v42/) - Penetration testing checklist
- [OWASP: Cheat Sheets](https://cheatsheetseries.owasp.org/)
- [OWASP: API Security](https://owasp.org/www-project-api-security/)

## Secure-by-design project
Secure-by-design project promotes:
- Security-first mindset
- Better project planning and estimation
- Preparation for penetration testing
- Compliance with gov't guidelines and regulations

Sample list of development tasks created following the tactics, guidelines and OWASP Cheat Sheets:
- [Issue board](https://gitlab.com/testudio/security-by-design-starter-kit/-/boards/5621064) - issues grouped by labels, eg Project management, Development, CI, API security
- [Issues](https://gitlab.com/testudio/security-by-design-starter-kit/-/issues)

## TL;DR;

This is a list of Drupal 9/10 modules to improve security following OWASP cheat sheets and gov't guidelines:

NOTE: simply installing and enabling these modules will not magically make your web application secure - it's required to consider carefully which modules are to be enabled and how are they configured specifically for your web application.

```
# Define security policy
composer require drupal/securitytxt

# Configure HTTP Headers
composer require drupal/seckit
composer require drupal/csp

# Prevent Web Application Fingerprinting
composer require drupal/remove_http_headers

# Protect file uploads from malware
composer require drupal/clamav

# Protect API endpoints:
# Restrict routes/fields
composer require drupal/jsonapi_extras
# Enable rate limiting (not compatible with Drupal 10)
composer require drupal/rate_limits

# Protect Authentication controls
composer require drupal/password_policy
composer require drupal/login_security
composer require drupal/flood_control
composer require drupal/autologout
composer require drupal/session_limit
composer require drupal/tfa
composer require drupal/username_enumeration_prevention

# Prevent spam/brute force attacks
composer require drupal/captcha

# Prevent exploitation of non-production features
composer require drupal/config_readonly

# Enable logging of any configuration changes
composer require drupal/config_log

# Review security configuration, access control, etc
composer require drupal/security_review
# (not compatible with Drupal 10)
composer require drupal/entity_access_audit

# Enable modules using Drush
drush en -y securitytxt
drush en -y seckit csp
drush en -y remove_http_headers
drush en -y clamav
drush en -y jsonapi_extras
drush en -y rate_limits
drush en -y captcha
drush en -y password_policy login_security flood_control autologout session_limit tfa username_enumeration_prevention
drush en -y config_readonly
drush en -y config_log
drush en -y security_review entity_access_audit


```

* _If you have recommendations for other modules - please create an issue!_

## Learn 

How to learn to to write secure code?
- coding standards
- security alerts/newsletters
- security community
- review/read code/fix of existing security issues

### Coding standards and best practices
- [PHP the right way](https://phptherightway.com/)
- [Clean code PHP](https://github.com/piotrplenik/clean-code-php)
- [Clean code JS](https://github.com/ryanmcdermott/clean-code-javascript)
- [Drupal: writing secure code](https://www.drupal.org/docs/security-in-drupal/writing-secure-code-for-drupal)
- [Drupal: JSON API module - security-considerations](https://www.drupal.org/docs/core-modules-and-themes/core-modules/jsonapi-module/security-considerations)
- [Drupal7: writing secure code](https://www.drupal.org/docs/7/security/writing-secure-code) - Learning on past mistakes!

### Subscribe to security alerts
- Drupal security: https://www.drupal.org/security  Drupal.org: Your profile >> Edit >> My newsletters tab
- Symfony: https://symfony.com/blog/category/security-advisories 
- Everything (used by SAST tools and `composer audit`): https://github.com/advisories (subset of CVE)
Based on [CVE](https://www.cve.org/) (Common Vulnerabilities and Exposures):
- AU ACSC: https://www.cyber.gov.au/about-us/view-all-content/alerts-and-advisories 
- NZ CERTNZ: https://www.cert.govt.nz/about/about-us/?subscribe/
- Germany: https://wid.cert-bund.de/portal/wid/kurzinformationen
- Austria: https://www.onlinesicherheit.gv.at/Services/Sicherheitswarnungen.html


### Get involved with community and conferences
- [Join OWASP](https://owasp.org/membership/)
- Attend a security conference (or watch presentations): 
  - [RSA](https://www.rsaconference.com/)
  - [OWASP Global](https://www.youtube.com/@OWASPGLOBAL/videos)
  - [NDC {Security}](https://www.youtube.com/watch?v=BkigVNNSurI&list=PL03Lrmd9CiGey4D3-wb_2SWTmLJtGHC_j)
- Attend your local OWASP meetup

### Training and certification
- [Google Cybersecurity Certificate](https://grow.google/certificates/cybersecurity)
- [SecureFlag (free with OWASP membership)](https://www.secureflag.com/owasp)
- [PortSwigger Web secuirty academy](https://portswigger.net/web-security)


## Practice

### Review Drupal Security team's issues and fixes
Schedule secuirty review sessions with your teammates: 
- select a recent secuirty issue (from Drupal Security or Github/advisories)
- view the corresponding commit: identify an issue and discuss the solution
- review your own codebase for potential exposure

#### Arbitrary PHP code execution
Drupal core - Critical - Arbitrary PHP code execution - SA-CORE-2022-014
- [Issue](https://www.drupal.org/sa-core-2022-014)
- [Solution](https://git.drupalcode.org/project/drupal/-/commit/ec39795b287ff0f1ce4e1870683b1bcde5574d8e)

#### Information Disclosure
Drupal core - Moderately critical - Information Disclosure - SA-CORE-2023-003
- [Issue](https://www.drupal.org/sa-core-2023-003)
- [Solution](https://git.drupalcode.org/project/drupal/-/commit/fb167909cfb7e109a44d79058d909558f52ca6e2)

#### Access bypass
Apigee Edge - Moderately critical - Access bypass - SA-CONTRIB-2022-045
- [Issue](https://www.drupal.org/sa-contrib-2022-045) 
- [Solution](https://github.com/apigee/apigee-edge-drupal/pull/709/files)

Taxonomy Manager - Moderately critical - Access bypass - SA-CONTRIB-2021-035
- [Issue](https://www.drupal.org/sa-contrib-2021-035)
- [Solution](https://git.drupalcode.org/project/taxonomy_manager/-/commit/aadb711befd3d32b598d27d5294693fab23fde03)

#### SQL Injection
Anti Spam by CleanTalk - Moderately critical - SQL Injection - SA-CONTRIB-2022-032
- [Issue](https://www.drupal.org/sa-contrib-2022-032) 
- [Solution](https://git.drupalcode.org/project/cleantalk/-/commit/b9036f022a8e4e959a44cf96557d61c89d09acb9#9f595910cf0add4985cd879df89669a53a7683f3_144_144) 

#### XSS
Xray Audit - Moderately critical - Cross site scripting - SA-CONTRIB-2023-012
- [Issue](https://www.drupal.org/sa-contrib-2023-012) 
- [Solution](https://git.drupalcode.org/project/xray_audit/-/commit/70fe578149d20a54f6730f5b23f11ba6385c342c#ebd26a2485114275624e4a0b97a91cd9c48943d2_368_368)

Lottiefiles Field - Moderately critical - Cross Site Scripting - SA-CONTRIB-2022-046
- [Issue](https://www.drupal.org/sa-contrib-2022-046)
- [Solution](https://git.drupalcode.org/project/lottiefiles_field/-/commit/b371effd1d778562c044a64233fabc5d455afcc1#bd096fc5937183b76d18c131c7b73a2d14f3f420_65_66)

#### Wordpress example
Access bypass on payment plugin - CVE-2023-28121
- [Issue](https://www.cve.org/CVERecord?id=CVE-2023-28121)
- [Patch on github](https://github.com/Automattic/woocommerce-payments/compare/5.6.1...5.6.2)

### Review a contrib module
Follow [OWASP: Code review guide](https://owasp.org/www-project-code-review-guide/)

Code review - module structure, Drupal coding standards, anything that's not quite right:
- are there any debug messages left in the code?
- how module is configured - can if be exported as `/config/`? How secrets are managed/stored?
- are there any specific permissions defined in this module? Are permissions being checked?
- is there any usage of direct database calls? (DB calls are very expensive and slow!)
- check *.routing.yml - are all private routes protected as required?
- check *.twig templates - any usages or `{{ value|raw }}` - there are only **3** usages of raw in templates in Drupal Core 10, there should be a really good reason to use `raw`!
- input validation - are there any usages of unescaped query string, DB queries, is data validated on server side before processing, etc?
- are dependencies up to date?
- how error handling and logging is performed?
- etc etc etc

Try: [Cloudflare](https://git.drupalcode.org/project/cloudflare/-/tree/2.0.x?ref_type=heads) vs [This module](https://www.drupal.org/project/userway)

>
> **Security issues are to be reported Drupal Security Advisory**
>
> Help Drupal comminity by reporting security issues and contributing by creating tasks/bugs if you find any other issues! 
> 
  

## References:
- [OWASP: Web Security Testing Guide (WSTG)](https://owasp.org/www-project-web-security-testing-guide/v42/) - Penetration testing checklist
- [OWASP: Cheat Sheets](https://cheatsheetseries.owasp.org/)
- [OWASP: API Security](https://owasp.org/www-project-api-security/)
- [ACSC (AUS): Guidelines for Software Development](https://www.cyber.gov.au/resources-business-and-government/essential-cyber-security/ism/cyber-security-guidelines/guidelines-software-development)
- [ACSC (AUS): Securing CMS](https://www.cyber.gov.au/resources-business-and-government/maintaining-devices-and-systems/system-hardening-and-administration/web-hardening/securing-content-management-systems)
- [CISA (USA): Secure-by-design](https://www.cisa.gov/resources-tools/resources/secure-by-design-and-default)
- [NCSC (UK): Secure development and deployment guidance](https://www.ncsc.gov.uk/collection/developers-collection)
- [OWASP: 10 Proactive Controls](https://owasp.org/www-project-proactive-controls/)
- [OWASP: Application Security Verification Standard (ASVS)](https://github.com/OWASP/ASVS) 
- [OWASP: Code review guide](https://owasp.org/www-project-code-review-guide/)

## Support
- Report bugs and request features in the Gitlab Issues.
- Use merge requests (MRs) to contribute to this project.

## License
CC0 1.0 Universal - CC0 public domain
